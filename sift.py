#!/usr/bin/env python3

import numpy as np
import cv2 as cv

# PLATZHALTER1.jpg und PLATZHALTER2.jpg müssen geändert werden
img1 = cv.imread('PLATZHALTER1.jpg',cv.IMREAD_GRAYSCALE) # Zu vergleichendes Bild
img2 = cv.imread('PLATZHALTER2.jpg',cv.IMREAD_GRAYSCALE) # Trainingsbild

# SIFT Detektor wird initialisiert
sift = cv.xfeatures2d.SIFT_create()

# Die KeyPoints und Deskriptoren werden gefunden
kp1, des1 = sift.detectAndCompute(img1,None)
kp2, des2 = sift.detectAndCompute(img2,None)

# BFMatcher Initialisierung
bf = cv.BFMatcher()
matches = bf.knnMatch(des1,des2,k=2)

# Überprüfung des Verhältnis der Matches
# Dafür wird ein bestimmte Schwelle gewählt
good = []
for m,n in matches:
    if m.distance < 0.55*n.distance:
        good.append([m])

# Zuletzt wird das entstehende Bild generiert
img3 = cv.drawMatchesKnn(img1,kp1,img2,kp2,good,None,flags=cv.DrawMatchesFlags_NOT_DRAW_SINGLE_POINTS)

cv.imwrite('sift_features.jpg', img3)
